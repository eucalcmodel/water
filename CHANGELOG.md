# Changelog

## Unreleased

### Added
- 

### Changes
- Update values of the warning: level 2 if WEI >= 0.4 for 3 regions (instead of 2); level 1 if WEI >= 0.2 for 5 regions (instead of 2). Based on a comparison with values in 2015 and in 2050 for EURef, L1 and L4 scenarios.
- Interface with Agriculture (Added fibres) + water fixed assumptions (fibres water factor) 

### Removed
- 

## 2.2

### Added
- pre-processing calculation workflow for "wat_fixed-assumptions" input file

### Changes
- changed the "wat_fixed-assumptions" input file

## 2.2

### Added
- wrapped "Testing EU28+1 historical calibration" into metanodes and saved the metanodes (I have created 2 metanodes: one for irrigation and one for all the others)
- column filters in section 4 (power generation water) to remove unnecessary columns along the flow
- new output to TPE with new port to "4.4 Water" metanode : wat_diff20502015_WEI-normal[-] (output ports were properly renamed). Added also on Googlesheet (0.2 Pathway Explorer, column A) :

### Changes
- Bigger font for blue box titles

### Removed
- removed all "Testing EU28+1 historical calibration" calculation boxes (for optimisation)
- removed Tree merge node for summing all electricity demands into total electricity demand (section 4)
- removed a joiner after calibration in section 4
- removed outputs to TPE from KNIME and on Googlesheet (0.2 Pathway Explorer, column A) : wat_total-water-consumption[m3] AND wat_water-availability[m3]


## 2.1
### Added
- Added a constant in inputs for irrigation water calculation
- Added a calculation step in Livestock, Irrigation and Energy sections (4.4 water metanode)that adapts water requirements values to regional specificities with correction factors
- Added corrections factors columns in the "fixed-assumptions" input file

### Changes
- variable names harmonization in input nodes and adapted subsequent calculation paths
- changed power generation calculation in parallel to calculation in series
- renamed inputs to Googlesheet metanode

### Removed
- back up 4.4 water metanode
- unused branches in calculation sections


## 1.9
### Added
- Climate excel reader in "Import data from other WP" section and added associated port on the 4.4 Water calculation node
- Water availability excel reader in "Scenarios & Historical Data IMPORT PASSENGER" and added associated port on the 4.4 Water calculation node (port 9)
- Semester shares excel reader in "Scenarios & Historical Data IMPORT PASSENGER" and added associated port on the 4.4 Water calculation node (port 10)
- Calculation for water stress (section 6) 
- Links to calibration output (doesn't work yet, didn't how to fiddle with googlesheet update node)
- Metanode for sub-regions splitting in section 6
- Metanode for formatting climate input in section 6
- Updated Ammonia interface (Boris)

### Changes

### Removed
- Old "WAT_water-resources" metanode for water availability import 

## 1.7
### Added
- metanode "WAT_Calibration-JRC" to calibrate on baseyear 2006 only 
(these calibration rates are then applied in the past and in the future)
- Data import metanode "WAT_livestock-factors", to import in right format livestock water factors 
that were externally calculated in columns through Python
-joiner in "Assemble columns for all levers" frame, to include imported livestock factors 
- workflow "EUCalc - livestock water requirements calculation"
- workflow "EUCalc - water resources calculation"
- EU28+1 level calibration for every calculation path (orange rectangles)

### Changes
- Modified all calculation paths to calibrate and use EUCalc metanodes instead of column aggregator and math formula
- Changed agriculture inputs and modified livestock and irrigation calculation paths accordingly.
- Used EUCalc unit conversion metanode in energy to convert TWh into GWh


## 1.6
### Added
- two workflows for Exiobase data extraction
- two workflows for FAO data extraction
- a workflow to calculate potential water factors from Exiobase and OGUT data
- a workflow to calculate irrigation shares (% of area)
- new materials in industry (alu, lime, glass) = new outputs for TPE
- electricity production from biomass in energy = new outputs to TPE
- 3 irrigation-demand outputs to TPE

### Changes
- Changed interface names with electricity supply, 
which implies that some nodes were removed in preprocessing of data before calculation path for elc
and added a column rename to obtain the same names as before to avoid modifying the rest of the nodes.
Adapted the units from TWh to GWh to fit calculation units.
- Changed Minerals excel reader into Climate excel reader -> changed also in googlesheet update metanode
- Updated water_man (+conf file) and fixed-assumptions input files


## 1.5
### Added
- calculation tree: water consumption and water withdrawal for power generation cooling
- calculation tree: water consumption and water withdrawal for manufacturing processes
- filters before calculation trees for household, agriculture, power generation cooling and manufacturing processes
- data import for water resources with ots and fts, filled with dummies (to be connected)


### Changes
- modified the agriculture calculation tree to output not only food, but also feed and bioenergy production water
- kept previous calculation tree from irrigation (3.) in case separate calculation is better than linear
- replaced min_wat file by new one sent by Morgan Raffray


### Removes


### Fixed 


## 1.4
### Added
- fixed assumptions node
- calculation tree: water consumption and water withdrawal for household
- calculation tree: water consumption and water withdrawal for agriculture (livestock & irrigation for food)

### Changes
- replaced "climate" excel writer by "ammonia" excel writer in WP inputs 

### Removes
- item

### Fixed 
- item

## 1.0
- Initial version, integrating all modules